package com.flowershop.model.action.fabricaction;

import com.flowershop.model.entity.Bouquet;
import com.flowershop.model.entity.Flower;
import com.flowershop.model.entity.Shop;
import com.flowershop.model.entity.enums.BouquetType;
import com.flowershop.model.entity.enums.FlowerType;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class BouquetActionValentine implements BouquetAction {

    @Override
    public Bouquet getBouquet(Shop shop) {
        List<Flower> flowers = new ArrayList<>();
        Map<FlowerType, Integer> shopFlowers = shop.getFlowers();
        flowers.add(new Flower(FlowerType.SUNFLOWER, 24));
        shopFlowers.remove(FlowerType.SUNFLOWER);
        flowers.add(new Flower(FlowerType.CHAMOMILE, 10));
        shopFlowers.remove(FlowerType.CHAMOMILE);
        flowers.add(new Flower(FlowerType.SNAPDRAGON, 20));
        shopFlowers.remove(FlowerType.SNAPDRAGON);
        flowers.add(new Flower(FlowerType.ORCHID, 23));
        shopFlowers.remove(FlowerType.ORCHID);
        int cost = 0;
        for (int i = 0; i < flowers.size(); i++) {
            cost += flowers.get(i).getCost();
        }
        return new Bouquet(flowers, cost, null,
                BouquetType.VALENTINE, null, 0);
    }
}
