package com.flowershop.model.action.fabricaction;

import com.flowershop.model.entity.Bouquet;
import com.flowershop.model.entity.Shop;

public interface BouquetAction {
    Bouquet getBouquet(Shop shop);
}
