package com.flowershop.model.action.fabricaction;

import com.flowershop.model.entity.Bouquet;
import com.flowershop.model.entity.Flower;
import com.flowershop.model.entity.Shop;
import com.flowershop.model.entity.enums.BouquetType;
import com.flowershop.model.entity.enums.FlowerType;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class BouquetActionFuneral implements BouquetAction {

    @Override
    public Bouquet getBouquet(Shop shop) {
        List<Flower> flowers = new ArrayList<>();
        Map<FlowerType, Integer> shopFlowers = shop.getFlowers();
        flowers.add(new Flower(FlowerType.TULIP, 24));
        shopFlowers.remove(FlowerType.TULIP);
        flowers.add(new Flower(FlowerType.SUNFLOWER, 14));
        shopFlowers.remove(FlowerType.SUNFLOWER);
        flowers.add(new Flower(FlowerType.ROSE, 43));
        shopFlowers.remove(FlowerType.ROSE);
        flowers.add(new Flower(FlowerType.SNAPDRAGON, 3));
        shopFlowers.remove(FlowerType.SNAPDRAGON);
        int cost = 0;
        for (int i = 0; i < flowers.size(); i++) {
            cost += flowers.get(i).getCost();
        }
        return new Bouquet(flowers, cost, null,
                BouquetType.FUNERAL, null, 0);
    }
}
