package com.flowershop.model.action.fabricaction;

import com.flowershop.model.entity.Bouquet;
import com.flowershop.model.entity.Flower;
import com.flowershop.model.entity.Shop;
import com.flowershop.model.entity.enums.BouquetType;
import com.flowershop.model.entity.enums.FlowerType;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class BouquetActionCustom implements BouquetAction {

    @Override
    public Bouquet getBouquet(Shop shop) {
        List<Flower> flowers = new ArrayList<>();
        Map<FlowerType, Integer> shopFlowers = shop.getFlowers();
        flowers.add(new Flower(FlowerType.SNAPDRAGON, 20));
        shopFlowers.remove(FlowerType.SNAPDRAGON);
        flowers.add(new Flower(FlowerType.CHAMOMILE, 10));
        shopFlowers.remove(FlowerType.CHAMOMILE);
        flowers.add(new Flower(FlowerType.ROSE, 20));
        shopFlowers.remove(FlowerType.ROSE);
        flowers.add(new Flower(FlowerType.TULIP, 23));
        shopFlowers.remove(FlowerType.TULIP);
        flowers.add(new Flower(FlowerType.ALSTROEMERIA, 23));
        shopFlowers.remove(FlowerType.ALSTROEMERIA);
        int cost = 0;
        for (int i = 0; i < flowers.size(); i++) {
            cost += flowers.get(i).getCost();
        }
        return new Bouquet(flowers, cost, null,
                BouquetType.CUSTOM, null, 0);
    }
}
