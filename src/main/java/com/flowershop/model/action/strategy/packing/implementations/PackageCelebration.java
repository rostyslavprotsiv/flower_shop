package com.flowershop.model.action.strategy.packing.implementations;

import com.flowershop.model.action.strategy.TestStrategies;
import com.flowershop.model.action.strategy.packing.PackageStrategy;
import com.flowershop.model.entity.Bouquet;
import com.flowershop.model.entity.enums.PackageTypes;

public class PackageCelebration implements PackageStrategy {
    private double cost;
    private PackageTypes packageType = PackageTypes.CELEBRATION;

    public void executePackaging() {
        TestStrategies.executingString("Celebration packaging");
    }

    public double getCost() {
        return cost;
    }

    public PackageTypes getPackageType() {
        return packageType;
    }

    @Override
    public void setPackage(Bouquet bouquet) {
        //TODO
    }
}
