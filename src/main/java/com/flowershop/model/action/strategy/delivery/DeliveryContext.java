package com.flowershop.model.action.strategy.delivery;

public class DeliveryContext {
    private DeliveryStrategy strategy;

    public DeliveryContext(DeliveryStrategy deliveryStrategy) {
        this.strategy = deliveryStrategy;
    }

    public void getDelivery() {

    }

    public void executeDelivery() {
        strategy.executeDelivery();
    }
}
