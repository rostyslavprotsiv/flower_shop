package com.flowershop.model.action.strategy.delivery.implementations;


import com.flowershop.model.action.strategy.TestStrategies;
import com.flowershop.model.action.strategy.delivery.DeliveryStrategy;
import com.flowershop.model.entity.Bouquet;
import com.flowershop.model.entity.enums.DeliveryType;

public class CityDelivery implements DeliveryStrategy {
    private double cost;
    private DeliveryType deliveryType = DeliveryType.CITY;

    public void setDelivery(Bouquet bouquet) {
        // TODO: 26.05.2019 implement delivery
    }

    public void executeDelivery() {
        TestStrategies.executingString("City delivery");
    }

    public double getCost() {
        return cost;
    }

    public DeliveryType getDeliveryType(Bouquet bouquet) {
        return deliveryType;
    }
}
