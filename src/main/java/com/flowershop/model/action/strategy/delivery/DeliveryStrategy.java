package com.flowershop.model.action.strategy.delivery;


import com.flowershop.model.entity.Bouquet;
import com.flowershop.model.entity.enums.DeliveryType;

public interface DeliveryStrategy {
    void setDelivery(Bouquet bouquet);
    double getCost();
    DeliveryType getDeliveryType(Bouquet bouquet);
    void executeDelivery();
}
