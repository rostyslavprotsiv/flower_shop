package com.flowershop.model.action.strategy.packing;

public class PackageContext {
    private PackageStrategy strategy;

    public PackageContext(PackageStrategy packageStrategy) {
        this.strategy = packageStrategy;
    }

    void getPackage() {
        // TODO: 26.05.2019 idk what is this
    }

    public void runPackage() {
        strategy.executePackaging();
    }
}
