package com.flowershop.model.action.strategy.packing;


import com.flowershop.model.entity.Bouquet;
import com.flowershop.model.entity.enums.PackageTypes;

public interface PackageStrategy {
    double getCost();

    PackageTypes getPackageType();

    void setPackage(Bouquet bouquet);

    void executePackaging();
}
