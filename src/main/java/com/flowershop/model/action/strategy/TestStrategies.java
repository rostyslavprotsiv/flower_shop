package com.flowershop.model.action.strategy;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TestStrategies {
    protected static final Logger LOGGER = LogManager.getLogger(TestStrategies.class);

    public static void executingString(String string) {
        LOGGER.info("Executing " + string + "...");
    }
}
