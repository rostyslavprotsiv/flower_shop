package com.flowershop.model.action.decorator;


import com.flowershop.model.entity.Bouquet;

public abstract class BouquetDecorator extends Bouquet {
    public Bouquet bouquet;

    public abstract Bouquet setDecorator(Bouquet bouquet, String value);
}
