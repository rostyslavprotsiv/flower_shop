package com.flowershop.model.action.decorator;


import com.flowershop.model.action.strategy.delivery.DeliveryContext;
import com.flowershop.model.action.strategy.delivery.implementations.CityDelivery;
import com.flowershop.model.action.strategy.delivery.implementations.UkraineDelivery;
import com.flowershop.model.action.strategy.delivery.implementations.WorldWideDelivery;
import com.flowershop.model.entity.Bouquet;
import com.flowershop.model.entity.enums.DeliveryType;

public class Delivery extends BouquetDecorator {
    private DeliveryContext context;

    @Override
    public Bouquet setDecorator(Bouquet bouquet, String value) {
        if (value.toUpperCase().equals(DeliveryType.CITY.name())) {
            context = new DeliveryContext(new CityDelivery());
        } else if (value.toUpperCase().equals(DeliveryType.UKRAINE.name())) {
            context = new DeliveryContext(new UkraineDelivery());
        } else if (value.toUpperCase().equals(DeliveryType.WORLD.name())) {
            context = new DeliveryContext(new WorldWideDelivery());
        } else if (value.toUpperCase().equals(DeliveryType.NONE.name())) {
            return bouquet;
        }
        context.executeDelivery();
        return bouquet;
    }
}
