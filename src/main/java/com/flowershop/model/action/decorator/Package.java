package com.flowershop.model.action.decorator;

import com.flowershop.model.action.strategy.packing.PackageContext;
import com.flowershop.model.action.strategy.packing.implementations.PackageCelebration;
import com.flowershop.model.action.strategy.packing.implementations.PackageFuneral;
import com.flowershop.model.action.strategy.packing.implementations.PackageSimple;
import com.flowershop.model.entity.Bouquet;
import com.flowershop.model.entity.enums.PackageTypes;

public class Package extends BouquetDecorator {
    private PackageContext context;

    @Override
    public Bouquet setDecorator(Bouquet bouquet, String value) {
        if (value.toUpperCase().equals(PackageTypes.CELEBRATION.name())) {
            context = new PackageContext(new PackageCelebration());
        } else if (value.toUpperCase().equals(PackageTypes.FUNERAL.name())) {
            context = new PackageContext(new PackageFuneral());
        } else if (value.toUpperCase().equals(PackageTypes.SIMPLE.name())) {
            context = new PackageContext(new PackageSimple());
        } else if (value.toUpperCase().equals(PackageTypes.NONE.name())) {
            return bouquet;
        }
        context.runPackage();
        return bouquet;
    }
}
