package com.flowershop.model.action;

import com.flowershop.model.entity.Shop;
import com.flowershop.model.entity.enums.*;
import com.flowershop.model.entity.observer.ChainOfShop;
import com.flowershop.model.entity.observer.Client;

import java.util.*;

public class WorkImitator {
    private List<Client> clients = new ArrayList<>();
    private ChainOfShop chainOfShops;
    private int clientId = 0;
    private final Random rand = new Random();

    public void addClient() {
        int cityNumber = rand.nextInt(3);
        City city = City.LVIV;
        if (cityNumber == 0) {
            city = City.KYIV;
        } else if (cityNumber == 1) {
            city = City.DNIPRO;
        } else if (cityNumber == 2) {
            city = City.ODESA;
        }
        Client client = new Client(clientId, NameGenerator.generateName(),
                rand.nextDouble() * 300, city, rand.nextBoolean(),
                new ArrayList<>(), Card.NONE_0);
        clientId++;
        clients.add(client);
    }

    public void addShops() {
        Map<FlowerType, Integer> flowers = new HashMap<>();
        int maxFlowers = 100;
        for (int i = 0; i < maxFlowers; i++) {
            flowers.put(FlowerType.ALSTROEMERIA, i);
            flowers.put(FlowerType.CHAMOMILE, i + 100);
            flowers.put(FlowerType.ORCHID, i + 200);
            flowers.put(FlowerType.ROSE, i + 300);
            flowers.put(FlowerType.SNAPDRAGON, i + 400);
            flowers.put(FlowerType.TULIP, i + 500);
            flowers.put(FlowerType.SUNFLOWER, i + 600);
        }
        List<Shop> shops = new LinkedList<>();
        shops.add(new Shop(0, City.LVIV, flowers));
        shops.add(new Shop(1, City.KYIV, flowers));
        shops.add(new Shop(2, City.DNIPRO, flowers));
        shops.add(new Shop(3, City.ODESA, flowers));
        chainOfShops = new ChainOfShop(clients, shops);
    }

    public void imitateBuying() {
        clients.forEach(cl->{
            Thread thread = new Thread(()-> {
                BouquetType bouquetType = BouquetType.WEDDING;
                PackageTypes packageTypes = PackageTypes.CELEBRATION;
                DeliveryType deliveryType = DeliveryType.NONE;
                int bouquetTypeNum = rand.nextInt(4);
                int packageTypeNum = rand.nextInt(2);
                int deliveryTypeNum = rand.nextInt(2);
                if(bouquetTypeNum == 0) {
                    bouquetType = BouquetType.BIRTHDAY;
                } else if(bouquetTypeNum == 1) {
                    bouquetType = BouquetType.MARCH_8;
                } else if(bouquetTypeNum == 2) {
                    bouquetType = BouquetType.VALENTINE;
                } else if(bouquetTypeNum == 3) {
                    bouquetType = BouquetType.FUNERAL;
                } else if(bouquetTypeNum == 4) {
                    bouquetType = BouquetType.CUSTOM;
                }
                if(packageTypeNum == 0) {
                    packageTypes = PackageTypes.FUNERAL;
                } else if(packageTypeNum == 1) {
                    packageTypes = PackageTypes.SIMPLE;
                } else if(packageTypeNum == 2) {
                    packageTypes = PackageTypes.NONE;
                }
                if(deliveryTypeNum == 0) {
                    deliveryType = DeliveryType.CITY;
                } else if(deliveryTypeNum == 1) {
                    deliveryType = DeliveryType.UKRAINE;
                } else if(deliveryTypeNum == 2) {
                    deliveryType = DeliveryType.WORLD;
                }
                chainOfShops.buyBouquet(cl, bouquetType.name(),
                        packageTypes.name(), deliveryType.name());
            });
            thread.start();
        });
    }


}
