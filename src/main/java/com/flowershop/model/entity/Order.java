package com.flowershop.model.entity;

import java.util.List;

public class Order {
    private List<Bouquet> bouquets;

    public Order() {
    }

    public Order(List<Bouquet> bouquets) {
        this.bouquets = bouquets;
    }

    public List<Bouquet> getBouquets() {
        return bouquets;
    }

    public void setBouquets(List<Bouquet> bouquets) {
        this.bouquets = bouquets;
    }
}
