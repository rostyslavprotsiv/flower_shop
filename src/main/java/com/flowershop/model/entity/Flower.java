package com.flowershop.model.entity;

import com.flowershop.model.entity.enums.FlowerType;

import java.util.Objects;

public class Flower {
    private FlowerType type;
    private double cost;

    public Flower(FlowerType type, double cost) {
        this.type = type;
        this.cost = cost;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public FlowerType getType() {
        return type;
    }

    public void setType(FlowerType type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "type = " + type.toString() + ", cost = " + cost;
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, cost);
    }

    @Override
    public boolean equals(Object obj) { // ...
        return super.equals(obj);
    }
}
