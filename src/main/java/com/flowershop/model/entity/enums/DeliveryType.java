package com.flowershop.model.entity.enums;

public enum DeliveryType {
    NONE, CITY, UKRAINE, WORLD
}
