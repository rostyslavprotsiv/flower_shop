package com.flowershop.model.entity.enums;

public enum PackageTypes {
    CELEBRATION, FUNERAL, SIMPLE, NONE
}
