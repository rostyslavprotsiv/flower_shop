package com.flowershop.model.entity.enums;

public enum Card {

    NONE_0(0),
    COMMON_3(1),
    PENSION_4(2),
    SLIVER_5(1000),
    GOLD_7(10000),
    PLATINUM(1000000);
    private int money_amount;

    Card(int money_amount) {
        this.money_amount = money_amount;
    }

    public int getMoney_amount() {
        return money_amount;
    }

    public void setMoney_amount(int money_amount) {
        this.money_amount = money_amount;
    }
}
