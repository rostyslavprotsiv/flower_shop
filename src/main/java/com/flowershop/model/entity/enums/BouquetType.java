package com.flowershop.model.entity.enums;

public enum BouquetType {
    WEDDING,
    BIRTHDAY,
    MARCH_8,
    VALENTINE,
    FUNERAL,
    CUSTOM;

    @Override
    public String toString() {

        return null;
    }
}
