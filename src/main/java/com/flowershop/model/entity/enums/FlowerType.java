package com.flowershop.model.entity.enums;

public enum FlowerType {
    CHAMOMILE,
    SUNFLOWER,
    ALSTROEMERIA,
    ROSE,
    TULIP,
    ORCHID,
    SNAPDRAGON;

    @Override
    public String toString() {
        if(this == CHAMOMILE) return "Chamomile";
        if(this == SUNFLOWER) return "Sunflower";
        if(this == ALSTROEMERIA) return "Alstroemeria";
        if(this == ROSE) return "Rose";
        if(this == TULIP) return "Tulip";
        if(this == ORCHID) return "Orchid";
        if(this == SNAPDRAGON) return "Snapdragon";
        return null;
    }
}
