package com.flowershop.model.entity.enums;

public enum City {
    LVIV,
    KYIV,
    DNIPRO,
    ODESA;
    @Override
    public String toString() {
        if(this == LVIV) return "Lviv";
        if(this == KYIV) return "Kyiv";
        if(this == DNIPRO) return "Dnipro";
        if(this == ODESA) return "Odesa";
        return null;
    }
}
