package com.flowershop.model.entity.fabricentity;

import com.flowershop.model.action.fabricaction.*;
import com.flowershop.model.entity.Bouquet;
import com.flowershop.model.entity.Shop;
import com.flowershop.model.entity.enums.BouquetType;
import com.flowershop.model.entity.observer.Client;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class FloristDnipro extends Florist{
    private final Logger LOGGER = LogManager.getLogger(FloristDnipro.class);

    public FloristDnipro(Shop shop, Client client) {
        super(shop, client);
    }

    @Override
    protected Bouquet assembleBouquet(BouquetType type, Shop shop) {
        BouquetAction bouquetAction;

        if(type.toString().equals("WEDDING")) {
            bouquetAction = new BouquetActionWedding();
        } else if(type.toString().equals("BIRTHDAY")) {
            bouquetAction = new BouquetActionBirthday();
        } else if(type.toString().equals("MARCH_8")) {
            LOGGER.info("Bouquet for 8 of March is not enable in Dnipro");
            return null;
        } else if(type.toString().equals("VALENTINE")) {
            bouquetAction = new BouquetActionValentine();
        } else if(type.toString().equals("FUNERAL")) {
            bouquetAction = new BouquetActionFuneral();
        } else if(type.toString().equals("CUSTOM")) {
            bouquetAction = new BouquetActionCustom();
        } else {
            return null;
        }
        return bouquetAction.getBouquet(shop);
    }
}
