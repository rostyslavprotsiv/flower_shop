package com.flowershop.model.entity.fabricentity;

import com.flowershop.model.action.decorator.BouquetDecorator;
import com.flowershop.model.action.decorator.Delivery;
import com.flowershop.model.action.decorator.Package;
import com.flowershop.model.entity.Bouquet;
import com.flowershop.model.entity.Order;
import com.flowershop.model.entity.Shop;
import com.flowershop.model.entity.enums.BouquetType;
import com.flowershop.model.entity.enums.Card;
import com.flowershop.model.entity.observer.Client;

import java.util.ArrayList;
import java.util.List;

public abstract class Florist {
    private Shop shop;
    private Client client;

    public Florist(Shop shop, Client client) {
        this.shop = shop;
        this.client = client;
    }

    public Order getOrder(BouquetType type, String pakage, String deliv) {
        Bouquet bouquet = assembleBouquet(type, shop);
        bouquet = decorateBouquet(bouquet, pakage, deliv);
        setDiscount(bouquet, client);
        List<Bouquet> bouquets = new ArrayList<>();
        bouquets.add(bouquet);
        return new Order(bouquets);
    }

    protected abstract Bouquet assembleBouquet(BouquetType type, Shop shop);

    private Bouquet decorateBouquet(Bouquet bouquet, String pakage, String deliv) {
        BouquetDecorator delivery = new Delivery();
        BouquetDecorator packagge =
                new Package();
        delivery.setDecorator(bouquet, deliv);
        packagge.setDecorator(delivery, pakage);
        return packagge;
    }

    private void setDiscount(Bouquet bouquet, Client client) {
        int allDiscounts = (int) bouquet.getCost();
        int percentsOfDiscount = 0;
        Card[] cards = Card.values();
        List<Order> orders = client.getProducts();
        List<Bouquet> bouquets;
        for (int i = 0; i < orders.size(); i++) {
            bouquets = orders.get(i).getBouquets();
            for (int j = 0; j < bouquets.size(); j++) {
                allDiscounts += bouquets.get(j).getCost();
            }
        }
        for (int i = 0; i < cards.length; i++) {
            if (cards[i].getMoney_amount() >= allDiscounts) {
                client.setCard(cards[i - 1]);
                if (i == 1) {
                    percentsOfDiscount = 3;
                } else if (i == 2) {
                    percentsOfDiscount = 4;
                } else if (i == 3) {
                    percentsOfDiscount = 5;
                } else if (i == 4) {
                    percentsOfDiscount = 7;
                } else if (i == 5) {
                    percentsOfDiscount = 15;
                }
                break;
            }
        }
        bouquet.setDiscount(percentsOfDiscount);
    }
}
