package com.flowershop.model.entity;

import com.flowershop.model.entity.enums.City;
import com.flowershop.model.entity.enums.FlowerType;

import java.util.Map;

public class Shop {
    private int id;
    private City location;
    private Map<FlowerType, Integer> flowers;

    public Shop(int id, City location, Map<FlowerType, Integer> flowers) {
        this.id = id;
        this.location = location;
        this.flowers = flowers;
    }

    public City getLocation() {
        return location;
    }

    public int getId() {
        return id;
    }

    public Map<FlowerType, Integer> getFlowers() {
        return flowers;
    }

    public void setFlowers(Map<FlowerType, Integer> flowers) {
        this.flowers = flowers;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setLocation(City location) {
        this.location = location;
    }
}
