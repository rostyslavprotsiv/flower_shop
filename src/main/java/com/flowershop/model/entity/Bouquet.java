package com.flowershop.model.entity;

import com.flowershop.model.entity.enums.BouquetType;
import com.flowershop.model.entity.enums.DeliveryType;
import com.flowershop.model.entity.enums.PackageTypes;

import java.util.List;

public class Bouquet {
    private List<Flower> bouquet;
    private double cost;
    private PackageTypes packing;
    private BouquetType type;
    private DeliveryType delivery;
    private int discount;

    public Bouquet() {
    }

    public Bouquet(List<Flower> bouquet, double cost, PackageTypes packing,
                   BouquetType type, DeliveryType delivery, int discount) {
        this.bouquet = bouquet;
        this.cost = cost;
        this.packing = packing;
        this.type = type;
        this.delivery = delivery;
        this.discount = discount;
    }

    public List<Flower> getBouquet() {
        return bouquet;
    }

    public double getCost() {
        return cost;
    }

    public BouquetType getType() {
        return type;
    }

    public PackageTypes getPacking() {
        return packing;
    }

    public DeliveryType getDelivery() {
        return delivery;
    }

    public int getDiscount() {
        return discount;
    }

    public void setBouquet(List<Flower> bouquet) {
        this.bouquet = bouquet;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public void setType(BouquetType type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object obj) { // ...
        return super.equals(obj);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    public void setPacking(PackageTypes packing) {
        this.packing = packing;
    }

    @Override
    public String toString() {
        return "bouquet = " + bouquet + ", cost = " + cost + ", packing = " + packing
                .toString() + ", bouquet = " + type.toString()
                + ", delivery = " + delivery
                .toString() + ", discount = " + discount;
    }

    public void setDelivery(DeliveryType delivery) {
        this.delivery = delivery;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }
}