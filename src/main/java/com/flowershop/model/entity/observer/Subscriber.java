package com.flowershop.model.entity.observer;

public interface Subscriber {
    void update(UpdateInfo info);
}
