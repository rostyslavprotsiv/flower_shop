package com.flowershop.model.entity.observer;

import com.flowershop.model.entity.Order;
import com.flowershop.model.entity.enums.Card;
import com.flowershop.model.entity.enums.City;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.Objects;

public class Client implements Subscriber {
    private int id;
    private String name;
    private double money;
    private City location;
    private Card card;
    private boolean extraDiscount;
    private List<Order> products;
    private final Logger LOGGER = LogManager.getLogger(Client.class);

    public Client(int id, String name, double money, City location,
                  boolean extraDiscount, List<Order> products, Card card) {
        this.id = id;
        this.name = name;
        this.money = money;
        this.location = location;
        this.extraDiscount = extraDiscount;
        this.products = products;
        this.card = card;
    }

    @Override
    public void update(UpdateInfo info) {
        LOGGER.info("id " + id + ", " + name + ", message: \"" + info
                .getMessage() + ", discount = " + info.isDiscount());
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }

    public City getLocation() {
        return location;
    }

    public void setLocation(City location) {
        this.location = location;
    }

    public boolean isExtraDiscount() {
        return extraDiscount;
    }

    public void setExtraDiscount(boolean extraDiscount) {
        this.extraDiscount = extraDiscount;
    }

    public List<Order> getProducts() {
        return products;
    }

    public void setProducts(List<Order> products) {
        this.products = products;
    }

    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        this.card = card;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Client client = (Client) o;
        return id == client.id &&
                Double.compare(client.money, money) == 0 &&
                extraDiscount == client.extraDiscount &&
                Objects.equals(name, client.name) &&
                location == client.location &&
                card == client.card &&
                Objects.equals(products, client.products) &&
                Objects.equals(LOGGER, client.LOGGER);
    }

    @Override
    public int hashCode() {
        return Objects
                .hash(id, name, money, location, card, extraDiscount, products, LOGGER);
    }

    @Override
    public String toString() {
        return "Client{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", money=" + money +
                ", location=" + location +
                ", card=" + card +
                ", extraDiscount=" + extraDiscount +
                ", products=" + products +
                ", LOGGER=" + LOGGER +
                '}';
    }
}
