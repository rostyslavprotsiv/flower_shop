package com.flowershop.model.entity.observer;

public class UpdateInfo {
    private String message;
    private boolean discount;

    public UpdateInfo() {
    }

    public UpdateInfo(String message, boolean discount) {
        this.message = message;
        this.discount = discount;
    }

    public String getMessage() {
        return message;
    }

    public boolean isDiscount() {
        return discount;
    }

    public void setDiscount(boolean discount) {
        this.discount = discount;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
