package com.flowershop.model.entity.observer;

import com.flowershop.controller.Controller;
import com.flowershop.model.entity.*;
import com.flowershop.model.entity.enums.BouquetType;
import com.flowershop.model.entity.enums.City;
import com.flowershop.model.entity.fabricentity.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public class ChainOfShop {
    private final Logger LOGGER = LogManager.getLogger(Controller.class);
    private List<Client> subscribers;
    private List<Shop> shops;

    public ChainOfShop() {
    }

    public ChainOfShop(List<Client> subscribers, List<Shop> shops) {
        this.subscribers = subscribers;
        this.shops = shops;
    }

    public void subscribe(Client subscriber) {
        subscribers.add(subscriber);
    }

    public void unsubscribe(Client subscriber) {
        subscribers.remove(subscriber);
    }

    public void notifyAll(UpdateInfo updateInfo) {
        LOGGER.info(updateInfo);
    }

    public void notifyConcreteSubscriber(Client subscriber, UpdateInfo updateInfo) {
        LOGGER.info("Message for : " + subscriber + " " + updateInfo);
    }

    public void notifyAboutExtraDiscount(UpdateInfo updateInfo) {
        LOGGER.info("Extra discount : " + updateInfo);
    }

    public void buyBouquet(Client client, String type, String pakage, String deliv) {
        Florist florist;
        Order order;
        City city = client.getLocation();
        BouquetType[] allTypes = BouquetType.values();
        switch (city.toString()) {
            case "LVIV":
                florist = new FloristLviv(shops.get(0), client);
                break;
            case "KYIV":
                florist = new FloristKyiv(shops.get(1), client);
                break;
            case "DNIPRO":
                florist = new FloristDnipro(shops.get(2), client);
                break;
            case "ODESA":
                florist = new FloristOdesa(shops.get(3), client);
                break;
            default:
                LOGGER.error("Such city doesn't exist");
                return;
        }
        for (BouquetType allType : allTypes) {
            if (type.toUpperCase().equals(allType.name())) {
                order = florist.getOrder(allType, pakage, deliv);
                client.getProducts().add(order);
                break;
            }
        }
    }
}
