package com.flowershop.view;

public final class Menu extends AbstractMenu {

    public Menu() {
        menu.put(1, "Exit");
        methodsForMenu.put(1, this::quit);
    }

    public void show() {
        out();
    }

    @Override
    protected void showInfo() {
        LOGGER.info("Menu for flower_Shop");
        menu.forEach((key, elem) -> LOGGER.info(key + " : " + elem));
    }
}
