package com.flowershop.view.interfaces;

@FunctionalInterface
public interface Printable {
    void print();
}
