package com.flowershop;

import com.flowershop.view.Menu;

public class Main {

    public static void main(final String[] args) {
        new Menu().show();
    }
}
